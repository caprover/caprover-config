# Deployment von Applikationen

Dieses Repository wurde im Rahmen einer Bachelorarbeit erstellt. Die Bachelorarbeit dient dazu, den Studenten an der THM eine einfache Möglichkeit zu geben, ihre entwickelten Applikationen in die Öffentlichkeit zu bringen. Das Hauptfeature wird es sein, dass bei jedem Commit in den Main Branch eine CI/CD Pipeline gestartet wird, welche ein Docker image aus dem Commit baut und direkt in Caprover deployed.
Zurzeit ist Caprover nur auf dem THM VPN verfügbar, um auf den Caprover Server zuzugreifen wird eine Verbindung zum VPN der THM benötigt.


# Was ist Caprover?
Ein kleiner Überblick auf der Caprover Homepage: [Caprover-Homepage](https://caprover.com/)

Caprover ist ein einfaches Tool, mit dem man in Sekunden Webapplikationen und Datenbanken deployen kann. Under the hood besteht Caprover aus Docker, Nginx und LetsEncrypt, LetsEncrypt ist eine kostenlose Möglichkeit , um für seine Domain ein SSL-Zertifikat zu bekommen.
Wir werden allerdings nicht diese Zertifikate benutzen, wir werden unsere eigenen Zertifikate von der THM nutzen.

# Wie deploye ich meine Applikationen

Zuerst muss man sich mit dem THM VPN verbinden, damnit man Zugriff auf den Server bekommt.
Nachdem die VPN-Verbindung steht, kann man nun im Webbrowser Caprover aufrufen unter captain.apps.mni.thm.de, das Passwort für Caprover lautet **captain42**.


<img src="https://i.ibb.co/pRVy9SP/Bildschirmfoto-2023-11-13-um-06-35-13.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="600" height="300">



Wenn alles geklappt hat befindet man sich nun auf dem Dashboard. Hier kann man nun auf den den Punkt Apps navigieren, um Apps zu deployen.

# Deployment einer Beispielapplikation mit Datenbank

## Deployment einer PostgreSQL Datenbank

Als Nächstes soll eine Applikation deployed werden, die eine Datenbank benutzt, um verschiedene Daten zu starten. Zu erst deployen wir eine PostgreSQL Datenbank. Dafür navigieren wir auf den Punkt Apps und klicken auf One-Click Apps/Databases. 



<img src="https://i.ibb.co/Vwz1HWF/Bildschirmfoto-2023-11-13-um-06-39-04.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="600" height="300">



Hier suchen wir nun nach PostgreSQL und wählen es aus. Im anschließenden Schritt gibt man jetzt nun noch an, wie die Applikation heißen soll, welches Docker Image Version verwenden werden soll und legt default database, Username und Passwort fest und klickt auf Deploy wenn man alles konfiguriert hat.



<img src="https://i.ibb.co/FHYqnHD/Bildschirmfoto-2023-11-13-um-06-43-40.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="600" height="300">


Wenn man nun seine Applikation mit der Datenbank verbinden will, spricht man die Datenbank an als srv-captain--postgres-db auf Port 5432.

# Deployment einer Applikation mit einer CI/CD-Pipeline

Nun kommen wir zum wichtigsten Punkt, wir deployen unsere Applikation auf Caprover.
Voraussetzung dafür ist, dass sich der Code der Applikation hier auf dem THM GitLab in einem Repository befindet und der Gruppe caprover angehört.

<img src="https://i.ibb.co/fpmSTQw/Bildschirmfoto-2023-11-13-um-12-52-46.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="800" height="300">

Zuerst öffnen wir Caprover im Browser unter captain.apps.mni.thm.de und navigieren auf Apps. Hier geben wir den Namen der App ein und erstellen die App.

Es wird ein Dockerfile benötigt, um den Container zu bauen. In diesem Repository wird es ein paar Beispiel Dockerfiles geben , die man verwenden/anpassen kann, wenn man einen anderen Tech Stack verwendet, wird man im Web auch fündig. **Ganz wichtig, dass Dockerfile heißt einfach nur Dockerfile** und sollte sich im root des Projektes befinden. 

Als Nächstes benötigen wir die Jobs für die Pipeline, die werden in der .gitlab-ci.yml definiert. Diese Datei befindet sich ebenfalls in diesem Repository, diese kann einfach ins eigene Repository kopiert werden.
CI/CD ist in GitLab per Default deaktiviert, dies kann man unter Settings->Visibility, project features, permissions->CI/CD aktivieren.


<img src="https://i.ibb.co/7kVXJgs/Bildschirmfoto-2023-11-13-um-12-28-52.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="800" height="300">


Das gitlab-ci file kann exakt so übernommen werden, das einzige, was man ändern sollte ist in Zeile 21. Zurzeit heißt das Image immer test-image, dieser Teil sollte in einen String geändert werden, der das Image identifiziert, dies könnte zum Beispiel der Name der Caprover Applikation sein.

Nachdem CI/CD aktiviert wurde, müssen noch die 3 CI/CD-Variablen angepasst werden, die in der Zeile 27 verwendet werden. Dies kann man unter Settings->CI/CD->Variables anpassen.

Hier werden 3 Variablen gesetzt, CAPROVER_APP, CAPROVER_PASSWORD und CAPROVER_URL:

<img src="https://i.ibb.co/Kr9Ytdz/Bildschirmfoto-2023-11-13-um-16-23-14.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="800" height="300">

CAPROVER_APP ist der Name der App, die vorher erstellt wurde. CAPROVER_PASSWORD ist das Passwort der Caprover Instanz, also wieder **captain42** und die Caprover_URL ist die Root-Domain captain.apps.mni.thm.de.

Falls nun alles richtig konfiguriert wurde, wird beim Push in den Main Branch die Pipeline getriggert und das Docker Image aus dem Code gebaut, wird in das private Registry gepusht, welches sich auf dem THM-Server befindet und wird anschließend auf Caprover deployed.

# Aktivierung von HTTPS für die Applikation

Um jetzt HTTPS zu aktivieren für die gerade erstellte Webapplikation, ruft man erneut das Dashboard im Browser auf und navigiert auf die Apps. Hier wählt man seine erstellte App aus. Anschließend klickt man auf den "Button Edit default Nginx Configurations" . 

<img src="https://i.ibb.co/0tk238B/Bildschirmfoto-2023-11-15-um-22-01-47.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="800" height="300">


In dem sich öffnenden Texteld, muss nun die Nginx Config angepasst werden. In diesem Repository gibt es die Datei nginx-config, die bereits die richtige Konfiguration enthält. Man kann sich einfach diese Konfiguration rauskopieren und in das Feld einfügen, dass sieht dann ungeähr so aus:

<img src="https://i.ibb.co/bLLCHYv/Bildschirmfoto-2023-11-15-um-22-05-50.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="800" height="300">

Nachdem die Config reinkopiert wurde, klickt man nur noch auf "Save & Restart" und die Nginx Konfiguration wird für die Applikation übernommen. Gratulation man hat soeben erfolgreich HTTPS für seine Applikation aktiviert.

# Aktivierung von automatischen Load Balancing

Mit Caprover kann man seine App skalieren, in dem man angibt, wie viele Instances man von seiner Applikation haben möchte. Falls eine Applikation down geht, wird automatisch eine andere Instanz der Applikation hochgefahren. Was ein weiteres geniales Feature ist, falls eure Applikation einen Request bekommt, wird er automatisch an eine der Instanzen der Applikationen geroutet, sodass man einen eingebauten Loadbalancer für seine Applikation bekommt.

Um zu konfigurieren, wie viele Instances man haben möchte, öffnet man im Browser wieder captain.apps.mni.thm.de, klickt auf den Button Apps und wählt seine Applikation aus. Unter dem Reiter App Configs, kann man nun konfigurieren, wie viele Instances von der Applikation bestehen sollen.

<img src="https://i.ibb.co/c6S4T9j/Bildschirmfoto-2023-11-17-um-11-16-11.png" alt="Bildschirmfoto-2023-11-13-um-12-52-46" border="0" width="800" height="300">
